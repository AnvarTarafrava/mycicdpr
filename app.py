from flask import Flask 

app = Flask(__name__)

@app.route('/')
def hello_name():
	return f"This is CI/CD Project for Anvar!!!"

@app.route('/hello')
def hello_world():
	return f"hello world!!!!"

if __name__ == "__main__":
	app.run(host="0.0.0.0",port=5000)

